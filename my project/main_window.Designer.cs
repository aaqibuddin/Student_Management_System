﻿namespace WinSMSdb
{
    partial class main_window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(main_window));
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.updatebtnedu = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.seduidtextBox = new System.Windows.Forms.TextBox();
            this.usntextBox1 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.SemcomboBox = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Submitbtn = new System.Windows.Forms.Button();
            this.yearscomboBox = new System.Windows.Forms.ComboBox();
            this.BranchcomboBox = new System.Windows.Forms.ComboBox();
            this.CoursecomboBox = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.updatebtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.fnametextBox = new System.Windows.Forms.TextBox();
            this.contacttextBox = new System.Windows.Forms.TextBox();
            this.agetextBox = new System.Windows.Forms.TextBox();
            this.emailtextBox = new System.Windows.Forms.TextBox();
            this.addresstextBox = new System.Windows.Forms.TextBox();
            this.usntextBox = new System.Windows.Forms.TextBox();
            this.surnametextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.submitbutton = new System.Windows.Forms.Button();
            this.femaleradioButton = new System.Windows.Forms.RadioButton();
            this.maleradioButton = new System.Windows.Forms.RadioButton();
            this.dobdateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.updsubtn = new System.Windows.Forms.Button();
            this.semcomboBox1 = new System.Windows.Forms.ComboBox();
            this.SubjectCodecomboBox1 = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.subsubmitbtn = new System.Windows.Forms.Button();
            this.subjectnametextBox = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.updatembtn = new System.Windows.Forms.Button();
            this.SubjectcodecomboBox = new System.Windows.Forms.ComboBox();
            this.msubmitbutton = new System.Windows.Forms.Button();
            this.FinalIAtextBox = new System.Windows.Forms.TextBox();
            this.S_IAtextBox = new System.Windows.Forms.TextBox();
            this.T_IAtextBox = new System.Windows.Forms.TextBox();
            this.F_IAtextBox = new System.Windows.Forms.TextBox();
            this.usnmtextBox = new System.Windows.Forms.TextBox();
            this.SMarksIDtextBox = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.logoutbtn = new System.Windows.Forms.Button();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Teal;
            this.tabPage2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabPage2.BackgroundImage")));
            this.tabPage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage2.Controls.Add(this.updatebtnedu);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.seduidtextBox);
            this.tabPage2.Controls.Add(this.usntextBox1);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.SemcomboBox);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.Submitbtn);
            this.tabPage2.Controls.Add(this.yearscomboBox);
            this.tabPage2.Controls.Add(this.BranchcomboBox);
            this.tabPage2.Controls.Add(this.CoursecomboBox);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(898, 421);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "StudentEducational";
            // 
            // updatebtnedu
            // 
            this.updatebtnedu.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updatebtnedu.Location = new System.Drawing.Point(506, 334);
            this.updatebtnedu.Name = "updatebtnedu";
            this.updatebtnedu.Size = new System.Drawing.Size(79, 29);
            this.updatebtnedu.TabIndex = 40;
            this.updatebtnedu.Text = "Update";
            this.updatebtnedu.UseVisualStyleBackColor = true;
            this.updatebtnedu.Click += new System.EventHandler(this.updatebtnedu_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(94, 74);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 20);
            this.label15.TabIndex = 39;
            this.label15.Text = "SeduID";
            // 
            // seduidtextBox
            // 
            this.seduidtextBox.Location = new System.Drawing.Point(246, 68);
            this.seduidtextBox.Name = "seduidtextBox";
            this.seduidtextBox.Size = new System.Drawing.Size(121, 20);
            this.seduidtextBox.TabIndex = 38;
            // 
            // usntextBox1
            // 
            this.usntextBox1.Location = new System.Drawing.Point(246, 106);
            this.usntextBox1.Name = "usntextBox1";
            this.usntextBox1.Size = new System.Drawing.Size(121, 20);
            this.usntextBox1.TabIndex = 36;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label19.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label19.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(94, 106);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(40, 20);
            this.label19.TabIndex = 16;
            this.label19.Text = "usn";
            // 
            // SemcomboBox
            // 
            this.SemcomboBox.FormattingEnabled = true;
            this.SemcomboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.SemcomboBox.Location = new System.Drawing.Point(246, 224);
            this.SemcomboBox.Name = "SemcomboBox";
            this.SemcomboBox.Size = new System.Drawing.Size(121, 22);
            this.SemcomboBox.TabIndex = 15;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label17.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(94, 224);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 20);
            this.label17.TabIndex = 14;
            this.label17.Text = "Sem";
            // 
            // Submitbtn
            // 
            this.Submitbtn.BackColor = System.Drawing.Color.Ivory;
            this.Submitbtn.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Submitbtn.Location = new System.Drawing.Point(369, 334);
            this.Submitbtn.Name = "Submitbtn";
            this.Submitbtn.Size = new System.Drawing.Size(75, 30);
            this.Submitbtn.TabIndex = 13;
            this.Submitbtn.Text = "Submit";
            this.Submitbtn.UseVisualStyleBackColor = false;
            this.Submitbtn.Click += new System.EventHandler(this.Submitbtn_Click);
            // 
            // yearscomboBox
            // 
            this.yearscomboBox.FormattingEnabled = true;
            this.yearscomboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.yearscomboBox.Location = new System.Drawing.Point(246, 266);
            this.yearscomboBox.Name = "yearscomboBox";
            this.yearscomboBox.Size = new System.Drawing.Size(121, 22);
            this.yearscomboBox.TabIndex = 9;
            // 
            // BranchcomboBox
            // 
            this.BranchcomboBox.FormattingEnabled = true;
            this.BranchcomboBox.Items.AddRange(new object[] {
            "Chemical Engineering",
            "Civil Engineering",
            "Computer Science &  Engineering",
            "Electrical & Electronics Engineering",
            "Electronic & Communication Engineering",
            "Information Science  & Engineering",
            "Mechanical Engineering",
            ""});
            this.BranchcomboBox.Location = new System.Drawing.Point(246, 186);
            this.BranchcomboBox.Name = "BranchcomboBox";
            this.BranchcomboBox.Size = new System.Drawing.Size(121, 22);
            this.BranchcomboBox.TabIndex = 8;
            // 
            // CoursecomboBox
            // 
            this.CoursecomboBox.FormattingEnabled = true;
            this.CoursecomboBox.Items.AddRange(new object[] {
            "Bachelor of Engineering(B.E)"});
            this.CoursecomboBox.Location = new System.Drawing.Point(246, 146);
            this.CoursecomboBox.Name = "CoursecomboBox";
            this.CoursecomboBox.Size = new System.Drawing.Size(121, 22);
            this.CoursecomboBox.TabIndex = 7;
            this.CoursecomboBox.SelectedIndexChanged += new System.EventHandler(this.CoursecomboBox_SelectedIndexChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label18.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(94, 266);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 20);
            this.label18.TabIndex = 6;
            this.label18.Text = "Years";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label14.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(94, 189);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 20);
            this.label14.TabIndex = 2;
            this.label14.Text = "Branch";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(94, 147);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(66, 20);
            this.label13.TabIndex = 1;
            this.label13.Text = "Course";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(379, 27);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(252, 27);
            this.label12.TabIndex = 0;
            this.label12.Text = "Educational Details";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.DarkCyan;
            this.tabPage1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabPage1.BackgroundImage")));
            this.tabPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage1.Controls.Add(this.updatebtn);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.fnametextBox);
            this.tabPage1.Controls.Add(this.contacttextBox);
            this.tabPage1.Controls.Add(this.agetextBox);
            this.tabPage1.Controls.Add(this.emailtextBox);
            this.tabPage1.Controls.Add(this.addresstextBox);
            this.tabPage1.Controls.Add(this.usntextBox);
            this.tabPage1.Controls.Add(this.surnametextBox);
            this.tabPage1.Controls.Add(this.nameTextBox);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.submitbutton);
            this.tabPage1.Controls.Add(this.femaleradioButton);
            this.tabPage1.Controls.Add(this.maleradioButton);
            this.tabPage1.Controls.Add(this.dobdateTimePicker);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(898, 421);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "SudentDetails";
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // updatebtn
            // 
            this.updatebtn.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updatebtn.Location = new System.Drawing.Point(556, 370);
            this.updatebtn.Name = "updatebtn";
            this.updatebtn.Size = new System.Drawing.Size(82, 32);
            this.updatebtn.TabIndex = 48;
            this.updatebtn.Text = "update";
            this.updatebtn.UseVisualStyleBackColor = true;
            this.updatebtn.Click += new System.EventHandler(this.updatebtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(324, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(351, 27);
            this.label1.TabIndex = 47;
            this.label1.Text = "Student Educational Details";
            // 
            // fnametextBox
            // 
            this.fnametextBox.Location = new System.Drawing.Point(235, 156);
            this.fnametextBox.Name = "fnametextBox";
            this.fnametextBox.Size = new System.Drawing.Size(165, 20);
            this.fnametextBox.TabIndex = 46;
            // 
            // contacttextBox
            // 
            this.contacttextBox.Location = new System.Drawing.Point(235, 354);
            this.contacttextBox.Name = "contacttextBox";
            this.contacttextBox.Size = new System.Drawing.Size(165, 20);
            this.contacttextBox.TabIndex = 39;
            // 
            // agetextBox
            // 
            this.agetextBox.Location = new System.Drawing.Point(235, 248);
            this.agetextBox.Name = "agetextBox";
            this.agetextBox.Size = new System.Drawing.Size(165, 20);
            this.agetextBox.TabIndex = 38;
            this.agetextBox.TextChanged += new System.EventHandler(this.agetextBox_TextChanged);
            // 
            // emailtextBox
            // 
            this.emailtextBox.Location = new System.Drawing.Point(235, 319);
            this.emailtextBox.Name = "emailtextBox";
            this.emailtextBox.Size = new System.Drawing.Size(165, 20);
            this.emailtextBox.TabIndex = 37;
            this.emailtextBox.TextChanged += new System.EventHandler(this.emailtextBox_TextChanged);
            // 
            // addresstextBox
            // 
            this.addresstextBox.Location = new System.Drawing.Point(235, 284);
            this.addresstextBox.Name = "addresstextBox";
            this.addresstextBox.Size = new System.Drawing.Size(165, 20);
            this.addresstextBox.TabIndex = 36;
            // 
            // usntextBox
            // 
            this.usntextBox.Location = new System.Drawing.Point(235, 56);
            this.usntextBox.Name = "usntextBox";
            this.usntextBox.Size = new System.Drawing.Size(165, 20);
            this.usntextBox.TabIndex = 35;
            // 
            // surnametextBox
            // 
            this.surnametextBox.Location = new System.Drawing.Point(235, 91);
            this.surnametextBox.Name = "surnametextBox";
            this.surnametextBox.Size = new System.Drawing.Size(165, 20);
            this.surnametextBox.TabIndex = 34;
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(235, 124);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(165, 20);
            this.nameTextBox.TabIndex = 33;
            this.nameTextBox.TextChanged += new System.EventHandler(this.nameTextBox_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(112, 156);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(107, 20);
            this.label11.TabIndex = 45;
            this.label11.Text = "FatherName";
            // 
            // submitbutton
            // 
            this.submitbutton.BackColor = System.Drawing.Color.Ivory;
            this.submitbutton.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitbutton.Location = new System.Drawing.Point(454, 370);
            this.submitbutton.Name = "submitbutton";
            this.submitbutton.Size = new System.Drawing.Size(81, 32);
            this.submitbutton.TabIndex = 44;
            this.submitbutton.Text = "Submit";
            this.submitbutton.UseVisualStyleBackColor = false;
            this.submitbutton.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // femaleradioButton
            // 
            this.femaleradioButton.AutoSize = true;
            this.femaleradioButton.BackColor = System.Drawing.Color.FloralWhite;
            this.femaleradioButton.Font = new System.Drawing.Font("Lucida Fax", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.femaleradioButton.Location = new System.Drawing.Point(329, 187);
            this.femaleradioButton.Name = "femaleradioButton";
            this.femaleradioButton.Size = new System.Drawing.Size(71, 20);
            this.femaleradioButton.TabIndex = 43;
            this.femaleradioButton.TabStop = true;
            this.femaleradioButton.Text = "Female";
            this.femaleradioButton.UseVisualStyleBackColor = false;
            // 
            // maleradioButton
            // 
            this.maleradioButton.AutoSize = true;
            this.maleradioButton.BackColor = System.Drawing.Color.FloralWhite;
            this.maleradioButton.Font = new System.Drawing.Font("Lucida Fax", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maleradioButton.Location = new System.Drawing.Point(235, 187);
            this.maleradioButton.Name = "maleradioButton";
            this.maleradioButton.Size = new System.Drawing.Size(55, 20);
            this.maleradioButton.TabIndex = 42;
            this.maleradioButton.TabStop = true;
            this.maleradioButton.Text = "Male";
            this.maleradioButton.UseVisualStyleBackColor = false;
            // 
            // dobdateTimePicker
            // 
            this.dobdateTimePicker.Location = new System.Drawing.Point(235, 222);
            this.dobdateTimePicker.Name = "dobdateTimePicker";
            this.dobdateTimePicker.Size = new System.Drawing.Size(165, 20);
            this.dobdateTimePicker.TabIndex = 41;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(113, 222);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 20);
            this.label10.TabIndex = 40;
            this.label10.Text = "DOB";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(113, 353);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 20);
            this.label9.TabIndex = 32;
            this.label9.Text = "Contact No";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(117, 250);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 20);
            this.label8.TabIndex = 31;
            this.label8.Text = "Age";
            this.label8.Click += new System.EventHandler(this.label8_Click_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(113, 318);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 20);
            this.label7.TabIndex = 30;
            this.label7.Text = "E-Mail";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(113, 281);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 20);
            this.label6.TabIndex = 29;
            this.label6.Text = "Address";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(113, 187);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 20);
            this.label5.TabIndex = 28;
            this.label5.Text = "Gender";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(112, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 20);
            this.label4.TabIndex = 27;
            this.label4.Text = "USN";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(112, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 20);
            this.label3.TabIndex = 26;
            this.label3.Text = "Surname";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(112, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 20);
            this.label2.TabIndex = 24;
            this.label2.Text = "Name";
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 42);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(906, 451);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabPage3.BackgroundImage")));
            this.tabPage3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage3.Controls.Add(this.updsubtn);
            this.tabPage3.Controls.Add(this.semcomboBox1);
            this.tabPage3.Controls.Add(this.SubjectCodecomboBox1);
            this.tabPage3.Controls.Add(this.label22);
            this.tabPage3.Controls.Add(this.subsubmitbtn);
            this.tabPage3.Controls.Add(this.subjectnametextBox);
            this.tabPage3.Controls.Add(this.label21);
            this.tabPage3.Controls.Add(this.label20);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(898, 421);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "SubjectDetails";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // updsubtn
            // 
            this.updsubtn.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updsubtn.Location = new System.Drawing.Point(523, 260);
            this.updsubtn.Name = "updsubtn";
            this.updsubtn.Size = new System.Drawing.Size(94, 32);
            this.updsubtn.TabIndex = 10;
            this.updsubtn.Text = "Update";
            this.updsubtn.UseVisualStyleBackColor = true;
            this.updsubtn.Click += new System.EventHandler(this.updsubtn_Click);
            // 
            // semcomboBox1
            // 
            this.semcomboBox1.FormattingEnabled = true;
            this.semcomboBox1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.semcomboBox1.Location = new System.Drawing.Point(304, 204);
            this.semcomboBox1.Name = "semcomboBox1";
            this.semcomboBox1.Size = new System.Drawing.Size(100, 22);
            this.semcomboBox1.TabIndex = 9;
            // 
            // SubjectCodecomboBox1
            // 
            this.SubjectCodecomboBox1.FormattingEnabled = true;
            this.SubjectCodecomboBox1.Items.AddRange(new object[] {
            "15CS51",
            "15CS52",
            "15CS53",
            "15CS54",
            "15CS553",
            "15CSL57",
            "15CSL58"});
            this.SubjectCodecomboBox1.Location = new System.Drawing.Point(304, 113);
            this.SubjectCodecomboBox1.Name = "SubjectCodecomboBox1";
            this.SubjectCodecomboBox1.Size = new System.Drawing.Size(100, 22);
            this.SubjectCodecomboBox1.TabIndex = 8;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label22.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(401, 24);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(183, 27);
            this.label22.TabIndex = 7;
            this.label22.Text = "Subject Details";
            // 
            // subsubmitbtn
            // 
            this.subsubmitbtn.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subsubmitbtn.Location = new System.Drawing.Point(401, 260);
            this.subsubmitbtn.Name = "subsubmitbtn";
            this.subsubmitbtn.Size = new System.Drawing.Size(78, 32);
            this.subsubmitbtn.TabIndex = 6;
            this.subsubmitbtn.Text = "Submit";
            this.subsubmitbtn.UseVisualStyleBackColor = true;
            this.subsubmitbtn.Click += new System.EventHandler(this.subsubmitbtn_Click);
            // 
            // subjectnametextBox
            // 
            this.subjectnametextBox.Location = new System.Drawing.Point(304, 164);
            this.subjectnametextBox.Name = "subjectnametextBox";
            this.subjectnametextBox.Size = new System.Drawing.Size(100, 20);
            this.subjectnametextBox.TabIndex = 4;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Lucida Fax", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(140, 208);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(42, 18);
            this.label21.TabIndex = 2;
            this.label21.Text = "Sem";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Lucida Fax", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(140, 166);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(117, 18);
            this.label20.TabIndex = 1;
            this.label20.Text = "SubjectName";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Lucida Fax", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(137, 117);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(111, 18);
            this.label16.TabIndex = 0;
            this.label16.Text = "SubjectCode";
            // 
            // tabPage4
            // 
            this.tabPage4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabPage4.BackgroundImage")));
            this.tabPage4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage4.Controls.Add(this.updatembtn);
            this.tabPage4.Controls.Add(this.SubjectcodecomboBox);
            this.tabPage4.Controls.Add(this.msubmitbutton);
            this.tabPage4.Controls.Add(this.FinalIAtextBox);
            this.tabPage4.Controls.Add(this.S_IAtextBox);
            this.tabPage4.Controls.Add(this.T_IAtextBox);
            this.tabPage4.Controls.Add(this.F_IAtextBox);
            this.tabPage4.Controls.Add(this.usnmtextBox);
            this.tabPage4.Controls.Add(this.SMarksIDtextBox);
            this.tabPage4.Controls.Add(this.label30);
            this.tabPage4.Controls.Add(this.label29);
            this.tabPage4.Controls.Add(this.label28);
            this.tabPage4.Controls.Add(this.label27);
            this.tabPage4.Controls.Add(this.label26);
            this.tabPage4.Controls.Add(this.label25);
            this.tabPage4.Controls.Add(this.label24);
            this.tabPage4.Controls.Add(this.label23);
            this.tabPage4.Location = new System.Drawing.Point(4, 26);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(898, 421);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "StudentMarks";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // updatembtn
            // 
            this.updatembtn.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updatembtn.Location = new System.Drawing.Point(585, 357);
            this.updatembtn.Name = "updatembtn";
            this.updatembtn.Size = new System.Drawing.Size(83, 28);
            this.updatembtn.TabIndex = 17;
            this.updatembtn.Text = "Update";
            this.updatembtn.UseVisualStyleBackColor = true;
            this.updatembtn.Click += new System.EventHandler(this.updatembtn_Click);
            // 
            // SubjectcodecomboBox
            // 
            this.SubjectcodecomboBox.FormattingEnabled = true;
            this.SubjectcodecomboBox.Items.AddRange(new object[] {
            "15CS51",
            "15CS52",
            "15CS53",
            "15CS54",
            "15CS553",
            "15CSL57",
            "15CSL58"});
            this.SubjectcodecomboBox.Location = new System.Drawing.Point(329, 158);
            this.SubjectcodecomboBox.Name = "SubjectcodecomboBox";
            this.SubjectcodecomboBox.Size = new System.Drawing.Size(100, 22);
            this.SubjectcodecomboBox.TabIndex = 16;
            // 
            // msubmitbutton
            // 
            this.msubmitbutton.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msubmitbutton.Location = new System.Drawing.Point(452, 357);
            this.msubmitbutton.Name = "msubmitbutton";
            this.msubmitbutton.Size = new System.Drawing.Size(79, 28);
            this.msubmitbutton.TabIndex = 15;
            this.msubmitbutton.Text = "Submit";
            this.msubmitbutton.UseVisualStyleBackColor = true;
            this.msubmitbutton.Click += new System.EventHandler(this.msubmitbutton_Click);
            // 
            // FinalIAtextBox
            // 
            this.FinalIAtextBox.Location = new System.Drawing.Point(329, 314);
            this.FinalIAtextBox.Name = "FinalIAtextBox";
            this.FinalIAtextBox.Size = new System.Drawing.Size(100, 20);
            this.FinalIAtextBox.TabIndex = 14;
            // 
            // S_IAtextBox
            // 
            this.S_IAtextBox.Location = new System.Drawing.Point(329, 241);
            this.S_IAtextBox.Name = "S_IAtextBox";
            this.S_IAtextBox.Size = new System.Drawing.Size(100, 20);
            this.S_IAtextBox.TabIndex = 13;
            // 
            // T_IAtextBox
            // 
            this.T_IAtextBox.Location = new System.Drawing.Point(329, 278);
            this.T_IAtextBox.Name = "T_IAtextBox";
            this.T_IAtextBox.Size = new System.Drawing.Size(100, 20);
            this.T_IAtextBox.TabIndex = 12;
            // 
            // F_IAtextBox
            // 
            this.F_IAtextBox.Location = new System.Drawing.Point(329, 202);
            this.F_IAtextBox.Name = "F_IAtextBox";
            this.F_IAtextBox.Size = new System.Drawing.Size(100, 20);
            this.F_IAtextBox.TabIndex = 11;
            this.F_IAtextBox.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // usnmtextBox
            // 
            this.usnmtextBox.Location = new System.Drawing.Point(329, 126);
            this.usnmtextBox.Name = "usnmtextBox";
            this.usnmtextBox.Size = new System.Drawing.Size(100, 20);
            this.usnmtextBox.TabIndex = 9;
            // 
            // SMarksIDtextBox
            // 
            this.SMarksIDtextBox.Location = new System.Drawing.Point(329, 86);
            this.SMarksIDtextBox.Name = "SMarksIDtextBox";
            this.SMarksIDtextBox.Size = new System.Drawing.Size(100, 20);
            this.SMarksIDtextBox.TabIndex = 8;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Lucida Handwriting", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(299, 21);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(272, 27);
            this.label30.TabIndex = 7;
            this.label30.Text = "Student Marks Details";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Lucida Fax", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(142, 316);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(69, 18);
            this.label29.TabIndex = 6;
            this.label29.Text = "FinalIA";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Lucida Fax", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(142, 280);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(73, 18);
            this.label28.TabIndex = 5;
            this.label28.Text = "ThirdIA";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Lucida Fax", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(142, 243);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(93, 18);
            this.label27.TabIndex = 4;
            this.label27.Text = "SescondIA";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Lucida Fax", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(142, 204);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(63, 18);
            this.label26.TabIndex = 3;
            this.label26.Text = "FirstIA";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Lucida Fax", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(142, 161);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(111, 18);
            this.label25.TabIndex = 2;
            this.label25.Text = "SubjectCode";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Lucida Fax", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(142, 128);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(41, 18);
            this.label24.TabIndex = 1;
            this.label24.Text = "USN";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Lucida Fax", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(142, 88);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(142, 18);
            this.label23.TabIndex = 0;
            this.label23.Text = "StudentMarksID";
            // 
            // logoutbtn
            // 
            this.logoutbtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.logoutbtn.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logoutbtn.Location = new System.Drawing.Point(816, 2);
            this.logoutbtn.Name = "logoutbtn";
            this.logoutbtn.Size = new System.Drawing.Size(78, 34);
            this.logoutbtn.TabIndex = 1;
            this.logoutbtn.Text = "Logout";
            this.logoutbtn.UseVisualStyleBackColor = true;
            this.logoutbtn.Click += new System.EventHandler(this.logoutbtn_Click);
            // 
            // main_window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(906, 467);
            this.Controls.Add(this.logoutbtn);
            this.Controls.Add(this.tabControl1);
            this.Name = "main_window";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.main_window_Load);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox seduidtextBox;
        private System.Windows.Forms.TextBox usntextBox1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox SemcomboBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button Submitbtn;
        private System.Windows.Forms.ComboBox yearscomboBox;
        private System.Windows.Forms.ComboBox BranchcomboBox;
        private System.Windows.Forms.ComboBox CoursecomboBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox fnametextBox;
        private System.Windows.Forms.TextBox contacttextBox;
        private System.Windows.Forms.TextBox agetextBox;
        private System.Windows.Forms.TextBox emailtextBox;
        private System.Windows.Forms.TextBox addresstextBox;
        private System.Windows.Forms.TextBox usntextBox;
        private System.Windows.Forms.TextBox surnametextBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button submitbutton;
        private System.Windows.Forms.RadioButton femaleradioButton;
        private System.Windows.Forms.RadioButton maleradioButton;
        private System.Windows.Forms.DateTimePicker dobdateTimePicker;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button subsubmitbtn;
        private System.Windows.Forms.TextBox subjectnametextBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox FinalIAtextBox;
        private System.Windows.Forms.TextBox S_IAtextBox;
        private System.Windows.Forms.TextBox T_IAtextBox;
        private System.Windows.Forms.TextBox F_IAtextBox;
        private System.Windows.Forms.TextBox usnmtextBox;
        private System.Windows.Forms.TextBox SMarksIDtextBox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button msubmitbutton;
        private System.Windows.Forms.ComboBox SubjectcodecomboBox;
        private System.Windows.Forms.ComboBox semcomboBox1;
        private System.Windows.Forms.ComboBox SubjectCodecomboBox1;
        private System.Windows.Forms.Button updsubtn;
        private System.Windows.Forms.Button updatebtn;
        private System.Windows.Forms.Button updatebtnedu;
        private System.Windows.Forms.Button updatembtn;
        private System.Windows.Forms.Button logoutbtn;
    }
}